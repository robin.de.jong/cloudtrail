package main

import (
	"cloudtrail/pkg/aws"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

const Dir = "../../files/logs/23"

func main() {
	// logs, err := aws.NewLog(Path)
	// if err != nil {
	// 	log.Fatal("Error opening logs: ", err)
	// }

	// for _, l := range logs.Records {
	// 	fmt.Println(l.EventTime, l.UserIdentity.UserName, l.EventSource, l.EventName, l.EventType)
	// }

	files, err := ioutil.ReadDir(Dir)
	if err != nil {
		log.Fatal("Error reading dir:", err)
	}

	for _, file := range files {
		name := file.Name()
		logs, err := aws.NewLog(Dir + "/" + name)
		if err != nil {
			log.Fatal(err)
		}
		checkLogs(logs)
	}
}

func checkLogs(logs *aws.AWSLog) {
	for _, l := range logs.Records {
		source := l.EventSource
		event := l.EventName

		if strings.Contains(source, "s3") && strings.Contains(strings.ToLower(event), "delete") {
			apns := getFilesFromLogs(l.Resources)
			fmt.Println(l.EventTime, l.UserIdentity.UserName, event, apns)
		}
	}
}

func getFilesFromLogs(resources []aws.Resource) []string {
	arns := []string{}

	for _, resource := range resources {
		arns = append(arns, resource.Arn)
	}

	return arns
}
