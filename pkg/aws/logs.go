package aws

import (
	"compress/gzip"
	"encoding/json"
	"io/ioutil"
	"os"
	"time"
)

type AWSLog struct {
	Records []struct {
		EventVersion string `json:"eventVersion"`
		UserIdentity struct {
			Type        string `json:"type"`
			PrincipalID string `json:"principalId"`
			Arn         string `json:"arn"`
			AccountID   string `json:"accountId"`
			AccessKeyID string `json:"accessKeyId"`
			UserName    string `json:"userName"`
		} `json:"userIdentity"`
		EventTime         time.Time `json:"eventTime"`
		EventSource       string    `json:"eventSource"`
		EventName         string    `json:"eventName"`
		AwsRegion         string    `json:"awsRegion"`
		SourceIPAddress   string    `json:"sourceIPAddress"`
		UserAgent         string    `json:"userAgent"`
		RequestParameters struct {
			ListType          string `json:"list-type"`
			BucketName        string `json:"bucketName"`
			ContinuationToken string `json:"continuation-token"`
			EncodingType      string `json:"encoding-type"`
			Prefix            string `json:"prefix"`
			Delimiter         string `json:"delimiter"`
			Host              string `json:"Host"`
		} `json:"requestParameters"`
		ResponseElements    any `json:"responseElements"`
		AdditionalEventData struct {
			SignatureVersion     string `json:"SignatureVersion"`
			CipherSuite          string `json:"CipherSuite"`
			BytesTransferredIn   int    `json:"bytesTransferredIn"`
			AuthenticationMethod string `json:"AuthenticationMethod"`
			XAmzID2              string `json:"x-amz-id-2"`
			BytesTransferredOut  int    `json:"bytesTransferredOut"`
		} `json:"additionalEventData"`
		RequestID          string     `json:"requestID"`
		EventID            string     `json:"eventID"`
		ReadOnly           bool       `json:"readOnly"`
		Resources          []Resource `json:"resources"`
		EventType          string     `json:"eventType"`
		ManagementEvent    bool       `json:"managementEvent"`
		RecipientAccountID string     `json:"recipientAccountId"`
		EventCategory      string     `json:"eventCategory"`
		TLSDetails         struct {
			TLSVersion               string `json:"tlsVersion"`
			CipherSuite              string `json:"cipherSuite"`
			ClientProvidedHostHeader string `json:"clientProvidedHostHeader"`
		} `json:"tlsDetails"`
	} `json:"Records"`
}

type Resource struct {
	Type      string `json:"type"`
	ARNPrefix string `json:"ARNPrefix,omitempty"`
	AccountID string `json:"accountId,omitempty"`
	Arn       string `json:"ARN,omitempty"`
}

func NewLog(path string) (*AWSLog, error) {
	_log := AWSLog{}

	bytes, err := _log.getLogUncompressed(path)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(bytes, &_log); err != nil {
		return nil, err
	}

	return &_log, nil
}

func (l *AWSLog) getLogUncompressed(path string) ([]byte, error) {
	reader, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	uncompressed, err := gzip.NewReader(reader)
	if err != nil {
		return nil, err
	}
	defer uncompressed.Close()

	content, err := ioutil.ReadAll(uncompressed)
	return content, err
}
